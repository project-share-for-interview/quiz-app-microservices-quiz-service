package com.hasith.quizservice.service;

import com.hasith.quizservice.dao.QuizDao;
import com.hasith.quizservice.entities.Question;
import com.hasith.quizservice.entities.QuestionWrapper;
import com.hasith.quizservice.entities.Quiz;
import com.hasith.quizservice.entities.Response;
import com.hasith.quizservice.feign.QuizInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class QuizService {

    QuizDao quizDao;
//    QuestionDao questionDao;

    QuizInterface quizInterface;

    @Autowired
    public QuizService(QuizDao quizDao,
                       QuizInterface quizInterface
    ) {
        this.quizDao = quizDao;
        this.quizInterface = quizInterface;

    }

    public ResponseEntity<String> createQuiz(String category, int numQ, String title) {


        List<Integer> questions = quizInterface.getQuestionsForQuiz(category, Integer.toString(numQ)).getBody();
        Quiz quiz = new Quiz();
        quiz.setTitle(title);
        quiz.setQuestions(questions);
        quizDao.save(quiz);

        return new ResponseEntity<>("Success", HttpStatus.CREATED);
    }

    public ResponseEntity<List<QuestionWrapper>> getQuizQuestions(Integer id) {
        Quiz quiz = quizDao.findById(id).get();
        List<Integer> questionIds = quiz.getQuestions();

        ResponseEntity<List<QuestionWrapper>> questions = quizInterface.getQuestionsFromId(questionIds);

        return questions;
    }

    public ResponseEntity<Integer> calculateResult(Integer id, List<Response> responses) {
        ResponseEntity<Integer> score = quizInterface.getScore(responses);
        return score;
    }
}
